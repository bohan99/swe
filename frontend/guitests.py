from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, os, sys


def selectDriver():
    envvar = os.environ.get("SELENIUM_DRIVER")
    if envvar is None:
        if sys.platform == "darwin":
            driver = "safari"
        elif sys.platform == "linux" or sys.platform == "linux32":
            driver = "firefox"
        else:
            driver = None
    else:
        driver = envvar

    if driver == "safari":
        return webdriver.Safari()
    elif driver == "firefox":
        return webdriver.Firefox()
    elif driver == "chrome":
        return webdriver.Chrome()
    else:
        raise ValueError("Invalid selenium driver choice")


class AcceptanceTest(unittest.TestCase):
    def setUp(self):
        # TODO: make this configurable so we can also use chrome and firefox
        self.driver = selectDriver()
        self.driver.implicitly_wait(30)
        self.base_url = "http://localhost:3000"

    def test_home_page(self):
        self.driver.get(self.base_url)

    def test_navbar(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Home").click()
        self.driver.find_element_by_link_text("Congresspeople").click()
        self.driver.find_element_by_link_text("Bills").click()
        self.driver.find_element_by_link_text("Committees").click()
        self.driver.find_element_by_link_text("Issue/Category").click()
        self.driver.find_element_by_link_text("About").click()

    def test_about_team_members(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("About").click()
        time.sleep(10)
        self.assertIsNotNone(
            self.driver.find_element_by_css_selector('img[alt="Bohan Zhang"]')
        )
        self.assertIsNotNone(
            self.driver.find_element_by_css_selector('img[alt="David Durairaj"]')
        )
        self.assertIsNotNone(
            self.driver.find_element_by_css_selector('img[alt="Dilan Galindo Reyna"]')
        )
        self.assertIsNotNone(
            self.driver.find_element_by_css_selector('img[alt="Souvik Banerjee"]')
        )
        self.assertIsNotNone(
            self.driver.find_element_by_css_selector('img[alt="Yuki Kameyama"]')
        )

    def check_image_alt_tags(self):
        time.sleep(10)
        images = self.driver.find_elements_by_tag_name("img")
        for i in images:
            self.assertIsNotNone(i.get_attribute("alt"))

    def test_about_images_alt_text(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("About").click()
        self.check_image_alt_tags()

    def test_congresspeople_images_alt_text(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Congresspeople").click()
        self.check_image_alt_tags()

    def test_bills_images_alt_text(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Bills").click()
        self.check_image_alt_tags()

    def test_committees_images_alt_text(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Committees").click()
        self.check_image_alt_tags()

    def test_issue_images_alt_text(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Issue/Category").click()
        self.check_image_alt_tags()

    def check_model_pagination(self):
        time.sleep(10)
        # Page 2
        self.driver.find_element_by_xpath('//*[@id="pagi"]/ul/li[4]/a').click()
        time.sleep(10)
        # First page
        self.driver.find_element_by_xpath('//*[@id="pagi"]/ul/li[1]/a').click()
        time.sleep(10)
        # third page
        self.driver.find_element_by_xpath('//*[@id="pagi"]/ul/li[5]/a').click()
        time.sleep(10)

    def test_congresspeople_pagination(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Congresspeople").click()
        self.check_model_pagination()

    def test_bills_pagination(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Bills").click()
        self.check_model_pagination()

    def test_committees_pagination(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Committees").click()
        self.check_model_pagination()

    def test_issue_pagination(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Issue/Category").click()
        self.check_model_pagination()

    def check_model_instance_page_link(self):
        time.sleep(10)
        previous_url = self.driver.current_url
        self.driver.find_element_by_xpath(
            '//*[contains(@class, "card-title")]//a/span/span'
        ).click()
        self.assertNotEqual(previous_url, self.driver.current_url)

    def test_congresspeople_instance_page_link(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Congresspeople").click()
        self.check_model_instance_page_link()

    def test_bills_instance_page_link(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Bills").click()
        time.sleep(10)
        previous_url = self.driver.current_url
        self.driver.find_element_by_xpath('//*[@id="pagi"]//table//a').click()
        self.assertNotEqual(previous_url, self.driver.current_url)

    def test_committees_instance_page_link(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Committees").click()
        self.check_model_instance_page_link()

    def test_issue_instance_page_link(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Issue/Category").click()
        self.check_model_instance_page_link()

    def test_sitewide_search(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/section/div/nav/ul/li[7]/div/div/div[1]/input'
        ).send_keys("Tammy", Keys.ENTER)

    def test_sitewide_search_button(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_xpath('//*[contains(@class, "btn-light")]').click()
        time.sleep(5)

    def test_sitewide_search_page(self):
        self.driver.get(self.base_url + "/search/test")
        self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/section/div/div/ul/li[1]/a'
        ).click()
        self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/section/div/div/ul/li[2]/a'
        ).click()
        self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/section/div/div/ul/li[3]/a'
        ).click()
        self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/section/div/div/ul/li[4]/a'
        ).click()

    def check_filter_reset_button(self):
        time.sleep(10)
        self.driver.find_element_by_xpath('//*[contains(@class, "btn-danger")]').click()

    def test_congresspeople_filter_reset(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Congresspeople").click()
        self.check_filter_reset_button()

    def test_bill_filter_reset(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Bills").click()
        self.check_filter_reset_button()

    def test_committee_filter_reset(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Committees").click()
        self.check_filter_reset_button()

    def test_issue_filter_reset(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Issue/Category").click()
        self.check_filter_reset_button()

    def check_filter_search(self):
        time.sleep(10)
        self.driver.find_element_by_xpath(
            '//*[@id="pagi"]/div[1]/div//div/div/div[1]/input[@placeholder="Search…"]'
        ).send_keys("xyz", Keys.ENTER)

    def test_congresspeople_filter_search(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Congresspeople").click()
        self.check_filter_search()

    def test_bill_filter_search(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Bills").click()
        self.check_filter_search()

    def test_committee_filter_search(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Committees").click()
        self.check_filter_search()

    def test_issue_filter_search(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Issue/Category").click()
        self.check_filter_search()

    def check_filter_search_button(self):
        time.sleep(10)
        self.driver.find_element_by_xpath(
            '//*[@id="pagi"]//*[contains(@class, "btn-light")]'
        ).click()
        time.sleep(5)

    def test_congresspeople_filter_search_button(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Congresspeople").click()
        self.check_filter_search_button()

    def test_bill_filter_search_button(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Bills").click()
        self.check_filter_search_button()

    def test_committee_filter_search_button(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Committees").click()
        self.check_filter_search_button()

    def test_issue_filter_search_button(self):
        self.driver.get(self.base_url)
        self.driver.find_element_by_link_text("Issue/Category").click()
        self.check_filter_search_button()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
