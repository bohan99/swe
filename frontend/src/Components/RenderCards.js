import React from 'react';
import styled from 'styled-components/macro';
import { Col, Row, Container } from 'reactstrap';

export default function RenderCards(props) {
  const cards = props.cards;
  const dataSize = cards.length;
  const maxPerRow = props.maxPerRow;
  const bootstrapGridWidth = 12;
  const elementWidth = Math.floor(bootstrapGridWidth / maxPerRow);
  const numRows = Math.ceil(dataSize / maxPerRow);
  const Margin = styled.div`
    margin: auto;
    padding: 7%;
    width: 100%;
    height: 100%;
  `;
  let rows = Array(numRows).fill().map((_, rowIndex) => {
    let dataIndex = maxPerRow * rowIndex;
    let cardsToDisplay = cards.slice(dataIndex, dataIndex + maxPerRow).map((card, cardIndex) => (<Col className="d-flex" key={`card-${cardIndex}`} sm={elementWidth}><Margin>{card}</Margin></Col>));
    return (
        <Row key={`row-${rowIndex}`}>
        {cardsToDisplay}
      </Row>
    );
  });
  return (<Container fluid={true} className="content-row">
          {rows}
          </Container>);
};
