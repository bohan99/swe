const jsonPromise = function (response) {
  return response.json();
};

const endpoint = !process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? "http://localhost:5000" : "https://api.congressexposed.me";

function apiFetch(apiURL) {
  return fetch(new URL(apiURL, endpoint).href).then(jsonPromise);
}

function absolutePath(p) {
  console.log(p);
  return endpoint + p;
}


export { apiFetch, endpoint, absolutePath, jsonPromise }
