export default function makeUserFriendlyLabel(attributeName) {
  let words = attributeName.split("_");
  if (words.length > 2 && words[2]==="passed"){
    words[0]=" ";
    
  }
  for (let i = 0; i < words.length; i++) {
    let w = words[i];
    if (w === "id") {
      words[i] = "ID";
    } else if (w.length > 0) {
      words[i] = w[0].toUpperCase() + w.substring(1);
    }
  }
  return words.join(" ");
};
