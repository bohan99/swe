import { apiFetch } from "./API.js";
import LRU from "lru-cache";

const parties = {
  valueToLabel: function (value) {
    if (value === "R") {
      return "Republican";
    } else if (value === "D") {
      return "Democrat";
    } else if (value === "I") {
      return "Independent";
    } else {
      return "None";
    }
  },
  values: ["R", "D", "I", null]
};

const congresses = {
  valueToLabel: function (value) { return value; },
  values: [115, 116]
};

const num_bills = {
  valueToLabel: function (value) { return value; },
  range: [0, 2800, 50],
};

const Models = {
  Bill: {
    attributes: {
      bill_id: { type: "string", filterable: true, sortable: true },
      short_title: { type: "string", filterable: true },
      full_title: { type: "string", filterable: true },
      summary: { type: "string" },
      house_passage_date: { type: "string", filterable: true, sortable: true },
      senate_passage_date: { type: "string", filterable: true, sortable: true },
      enacted_date: { type: "string", filterable: true, sortable: true },
      vetoed_date: { type: "string", filterable: true, sortable: true },
      congress: { type: "int", filterable: true, allowedValues: congresses, sortable: true },
    },
    relationships: {
      cosponsors: { type: "hasMany", model: "Member", inverse: "cosponsored_bills", inverseType: "hasMany" },
      sponsored_by: { type: "hasOne", model: "Member", inverse: "sponsored_bills" },
      committees: { type: "hasMany", model: "Committee", inverse: "bills", inverseType: "hasMany"},
      subcommittees: { type: "hasMany", model: "Subcommittee", inverse: "bills", inverseType: "hasMany"},
      issues: { type: "hasMany", model: "Issue", inverse: "bills", inverseType: "hasMany" }
    },
    key: "bill_id",
  },
  Committee: {
    attributes: {
      committee_id: { type: "int" },
      committee_code: { type: "string", filterable: true, sortable: true },
      congress: { type: "int", filterable: true, allowedValues: congresses, sortable: true },
      name: { type: "string", filterable: true, sortable: true },
      website: { type: "string", filterable: true },
      chair_party: { type: "string", filterable: true, allowedValues: parties }
    },
    relationships: {
      member_roles: { type: "hasMany", model: "MemberRoleCommittee", inverse: "MemberRole", inverseType: "hasMany" },
      members: { type: "hasMany", model: "Member" } /* no inverse! */
    },
    key: "committee_id",
  },
  Subcommittee: {
    attributes: {
      subcommittee_id: { type: "int" },
      subcommittee_code: { type: "string" },
      parent_committee_id: { type: "int" },
      congress: { type: "int" },
      name: { type: "string" },
      website: { type: "string" },
      chair_party: { type: "string" }
    },
    relationships: {
      parent_committee: { type: "hasOne", model: "Committee", inverse: "subcommittees" }
    },
    key: "subcommittee_id",
  },
  Issue: {
    attributes: {
      issue_id: { type: "int" },
      name: { type: "string", filterable: true, sortable: true },
      lobbying: { type: "string", filterable: true, sortable: true },
      num_bills: { type: "int", sortable: true, filterable: true, allowedValues: num_bills },
      num_bills_passed: { type: "int", sortable: true, filterable: true, allowedValues: num_bills },
      picture_url: { type: "string" },
    },
    relationships: {},
    key: "issue_id",
  },
  Member: {
    attributes: {
      member_id: { type: "string", filterable: true, sortable: true },
      first_name: { type: "string", filterable: true, sortable: true },
      middle_name: { type: "string", filterable: true },
      last_name: { type: "string", filterable: true, sortable: true },
      twitter_account: { type: "string", filterable: true },
      facebook_account: { type: "string", filterable: true },
      party: { type: "string", filterable: true, allowedValues: parties },
      biography: { type: "string", filterable: true },
      date_of_birth: { type: "string" },
      website: { type: "string" },
      picture_url: { type: "string" },
    },
    relationships: {
      roles: { type: "hasMany", model: "MemberRole", inverse: "member" },
      related_issues: { type: "hasMany", model: "Issue", inverse: "members", inverseType: "hasMany"}
    },
    key: "member_id",
  },
  MemberRole: {
    attributes: {
      member_role_id: { type: "int" },
      member_id: { type: "string" },
      congress: { type: "int" },
      chamber: { type: "string" },
      title: { type: "string" },
      short_title: { type: "string" },
      party: { type: "string" },
      leadership_role: { type: "string" },
      district: { type: "int" },
      state: { type: "string" },
      missed_votes_percent: { type: "float" },
      votes_with_party_percent: { type: "float" },
      start_date: { type: "string" },
      end_date: { type: "string" },
    },
    relationships: {
      committee_assignments: { type: "hasMany", model: "MemberRoleCommittee", inverse: "member_role" },
      subcommittee_assignments: { type: "hasMany", model: "MemberRoleSubcommittee", inverse: "member_role" }
    },
    key: "member_role_id",
  },
  MemberRoleCommittee: {
    attributes: {
      member_role_committee_id: { type: "int" },
      member_role_id: { type: "int" },
      committee_id: { type: "int" },
      title: { type: "string" },
      rank_in_party: { type: "int" }
    },
    relationships: {
      committee: { type: "hasOne", model: "Committee", inverse: "member_roles" }
    },
    key: "member_role_committee_id",
  },
  MemberRoleSubcommittee: {
    attributes: {
      member_role_subcommittee_id: { type: "int" },
      member_role_id: { type: "int" },
      subcommittee_id: { type: "int" },
      title: { type: "string" },
      rank_in_party: { type: "int" }
    },
    relationships: {
      subcommittee: { type: "hasOne", model: "Subcommittee", inverse: "member_roles" }
    },
    key: "member_role_subcommittee_id",
  },
  RollCall: {
    attributes: {
      rollcall_id: { type: "int" },
      bill_id: { type: "string" },
      roll_call_date: { type: "string" },
      session_number: { type: "int" },
      chamber: { type: "string" },
      roll_call_number: { type: "int" },
      congress: { type: "int" },
    },
    relationships: {
      bill: { type: "hasOne", model: "Bill", inverse: "rollcalls" }
    },
    key: "rollcall_id",
  },
  RollCallPosition: {
    attributes: {
      rollcallposition_id: { type: "int" },
      rollcall_id: { type: "int" },
      member_id: { type: "string" },
      vote_position: { type: "string" },
      dw_nominate: { type: "float" },
      roll_call_date: { type: "string" },
    },
    relationships: {
      rollcall: { type: "hasOne", model: "RollCall", inverse: "positions" },
      member: { type: "hasOne", model: "Member", inverse: "roll_call_positions" }
    },
    key: "rollcallposition_id",
  }
};

// Make inverse relationship links
for (let modelName in Models) {
  let model = Models[modelName];
  for (let relationshipName in model.relationships) {
    let relationship = model.relationships[relationshipName];
    if (relationship.inverse === undefined) { continue; }
    const relationshipModelName = relationship.model;
    let relationshipModel = Models[relationshipModelName];
    const inverseRelationshipType = (relationship.inverseType === undefined ? (relationship.type === "hasOne" ? "hasMany" : "hasOne") : relationship.inverseType);
    if (relationshipModel.relationships === undefined) {
      relationshipModel.relationships = {};
    }
    relationshipModel.relationships[relationship.inverse] = { type: inverseRelationshipType, model: modelName };
  }
}


// https://stackoverflow.com/questions/8834126/how-to-efficiently-check-if-variable-is-array-or-object-in-nodejs-v8
const isArray = function(a) {
  return (!!a) && (a.constructor === Array);
};

const isObject = function(a) {
  return (!!a) && (a.constructor === Object);
};

let modelFactories = {};
for (let modelName in Models) {
  let model = Models[modelName];
  modelFactories[modelName] = ((function (capturedModelName, capturedModel) {

    class ModelSingleRelationship {
      constructor (relationshipName, relationshipData) {
        this.relationshipName = relationshipName;
        this.relationshipData = relationshipData;
      }

      get () {
        if (this.relationshipData === null) {
          return Promise.resolve(null);
        } else {
          const linkedModel = capturedModel.relationships[this.relationshipName].model;
          if (isObject(this.relationshipData)) {
            return modelFactories[linkedModel].constructAndSave(this.relationshipData);
          } else {
            // relationshipData is an id
            return modelFactories[linkedModel].fromID(this.relationshipData);
          }
        }
      }
    };

    class ModelManyRelationship {
      constructor (relationshipName, relationshipData) {
        this.relationshipName = relationshipName;
        this.relationshipData = relationshipData.map((d) => new ModelSingleRelationship(relationshipName, d));
      }

      getAtIndex (index) {
        return this.relationshipData[index].get();
      }

      length () {
        return this.relationshipData.length;
      }

      asArray() {
        return this.relationshipData.map((d) => d.get());
      }
    };

    function validateAttributes(unvalidatedAttributes) {
      let attributes = {};
      for (let expectedAttributeName in capturedModel.attributes) {
        if (unvalidatedAttributes[expectedAttributeName] === undefined) {
          return null;
        }
        attributes[expectedAttributeName] = unvalidatedAttributes[expectedAttributeName];
      }
      return attributes;
    };

    function validateRelationships(unvalidatedRelationships) {

      let relationships = {};
      for (let expectedRelationshipName in capturedModel.relationships) {
        // make sure the relationship data we get contains the expected relationship
        if (unvalidatedRelationships[expectedRelationshipName] === undefined) {
          console.log("Did not match", expectedRelationshipName);
          return null;
        }

        const modelRelationshipSpec = capturedModel.relationships[expectedRelationshipName];

        // make sure type matches
        if (modelRelationshipSpec.type === "hasOne" && (unvalidatedRelationships[expectedRelationshipName] !== null && isArray(unvalidatedRelationships[expectedRelationshipName]))) {
          console.log("Did not match", expectedRelationshipName, "hasOne");
          return null;
        }

        if (modelRelationshipSpec.type === "hasMany" && !isArray(unvalidatedRelationships[expectedRelationshipName])) {
          console.log("Did not match", expectedRelationshipName, "hasMany");
          return null;
        }

        if (modelRelationshipSpec.type === "hasOne") {
          relationships[expectedRelationshipName] = new ModelSingleRelationship(expectedRelationshipName, unvalidatedRelationships[expectedRelationshipName]);
        } else {
          relationships[expectedRelationshipName] = new ModelManyRelationship(expectedRelationshipName, unvalidatedRelationships[expectedRelationshipName]);
        }
      }
      return relationships;
    };

    function modelID(validatedAttributes) {
      return validatedAttributes[capturedModel.key];
    };

    class ModelClass {
      constructor (attributes, relationships) {
        this.attributes = attributes;
        this.relationships = relationships;
      }

      getAttribute (attributeName) {
        return this.attributes[attributeName];
      }

      getRelationship (relationshipName) {
        return this.relationships[relationshipName];
      }

      static name () {
        return capturedModelName;
      }

      instanceID () {
        return modelID(this.attributes);
      }
    };

    class SlimModelClass {
      constructor (attributes) {
        this.attributes = attributes;
      }

      getAttribute (attributeName) {
        return this.attributes[attributeName];
      }

      static name () {
        return capturedModelName;
      }

      instanceID () {
        return modelID(this.attributes);
      }

      makeFullInstance () {
        return modelFactories[this.name()].fromID(this.instanceID());
      }
    };


    function construct (data) {
      let attributes = validateAttributes(data.attributes);
      let relationships = validateRelationships(data.relationships);
      if (attributes !== null && relationships !== null) {
        return new ModelClass(attributes, relationships);
      } else {
        return null;
      }
    }

    function constructSlim (data) {
      let attributes = validateAttributes(data.attributes);
      if (attributes !== null) {
        return new SlimModelClass(attributes);
      } else {
        console.log("failed to construct slim");
        return null;
      }
    }


    let instanceCache = new LRU({ max: 500 });
    let promiseCache = new LRU({ max: 500 });

    function constructAndSave (data) {
      let modelInstance = construct(data);
      if (modelInstance === null) {
        return null;
      } else {
        instanceCache.set(modelInstance.instanceID(), modelInstance);
        return modelInstance;
      }
    }

    function fromID (instanceID) {
      let cachedPromise = promiseCache.get(instanceID);
      if (cachedPromise !== undefined) {
        return cachedPromise;
      } else {
        let cachedInstance = instanceCache.get(instanceID);
        if (cachedInstance !== undefined) {
          let resultPromise = Promise.resolve(cachedInstance);
          promiseCache.set(instanceID, resultPromise);
          return resultPromise;
        } else {
          let resultPromise = apiFetch(`/api/${capturedModelName.toLowerCase()}/${instanceID}`).then(function (response) {
            return constructAndSave(response);
          });
          promiseCache.set(instanceID, resultPromise);
          return resultPromise;
        }
      }
    }

    function objectIsEmpty(obj) {
      for(let key in obj) {
        if(obj.hasOwnProperty(key))
          return false;
      }
      return true;
    }

    class Pagination {
      constructor (pageSize, filter, useCompleteData) {
        this.pageSize = pageSize;
        this.filter = filter;
        this._pageCache = new LRU({ max: 500 });
        this._numberOfPages = null;
        this.useCompleteData = true;
      }

      _internalGetPage (pageNumber) {
        let cached = this._pageCache.get(pageNumber);
        if (cached !== undefined) {
          return cached;
        } else {
          let params = new URLSearchParams();
          params.append("results_per_page", this.pageSize);
          params.append("page", pageNumber);
          let result;
          if (!objectIsEmpty(this.filter)) {
            result = apiFetch(`/api/${capturedModelName.toLowerCase()}/filter[objects]=[${JSON.stringify(this.filter)}]?${params.toString()}`);
          } else {
            result = apiFetch(`/api/${capturedModelName.toLowerCase()}?${params.toString()}`);
          }
          this._pageCache.set(pageNumber, result);
          return result;
        }
      }

      getPage (pageNumber) {
        return this._internalGetPage(pageNumber).then(function (response) {
          let result;
          if (!this.useCompleteData) {
            result = response.objects.map((d) => constructSlim(d));
          } else {
            result = response.objects.map((d) => constructAndSave(d));
          }
          if (pageNumber < response.total_pages) {
            this._internalGetPage(pageNumber + 1);
          }
          return result;
        }.bind(this));
      }

      totalPages () {
        if (this._numberOfPages !== null) {
          return Promise.resolve(this._numberOfPages);
        } else {
          return this._internalGetPage(1).then(function (response) {
            this._numberOfPages = response.total_pages;
            return this._numberOfPages;
          }.bind(this));
        }
      }
    }

    function rawSchema() {
      return capturedModel;
    }

    return {
      fromID, Pagination, rawSchema
    };

  })(modelName, model));
}


const Bill = modelFactories["Bill"];
const Committee = modelFactories["Committee"];
const Issue = modelFactories["Issue"];
const Member = modelFactories["Member"];

export { Bill, Committee, Issue, Member };
