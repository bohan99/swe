import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Redirect, Switch } from 'react-router-dom';
import { Container, Nav, NavItem, NavLink, Navbar, NavbarBrand, NavbarToggler, Collapse } from 'reactstrap';
import './App.css';
import About from './Pages/About/About.js';
import Congresspeople from "./Pages/Congresspeople/Congresspeople.js";
import Bills from "./Pages/Bills/Bills.js";
import Committees from "./Pages/Committees/Committees.js";
import Issue from "./Pages/Issue/Issue.js";
import Home from "./Pages/Home/Home.js";
import Visualization from "./Pages/Visualization/Visualization.js";
import SiteSearch from "./Pages/SiteSearch/SiteSearch.js";
import Background from "./Components/background.png";
import Search from "./Components/Search.js";

import 'bootstrap/dist/css/bootstrap.min.css';
// We need to import styled-components from styled-components/macro
// to make the babel macro run.
// eslint-disable-next-line
import styled from 'styled-components/macro';

var sectionStyle = {
  width: "auto",
  height: "auto",
  backgroundImage: `url(${Background})`,
  backgroundRepeat: "repeat"
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: null,
    };
    this.redirectSearch = this.redirectSearch.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
  }

  redirectSearch(searchTerm) {
    this.setState({searchTerm: searchTerm});

  }

  clearSearch(searchTerm) {
    this.setState({searchTerm: null});
  }

  render() {
    let pages = [
      (<Route key={0} exact path="/" component={Home} />),
      (<Route key={1} path="/Home" component={Home} />),
      (<Route key={2} path="/Congresspeople" component={Congresspeople} />),
      (<Route key={3} path="/Bills" component={Bills} />),
      (<Route key={4} path="/Committees" component={Committees} />),
      (<Route key={5} path="/Issue_Category" component={Issue} />),
      (<Route key={6} path="/search" component={SiteSearch} />),
      (<Route key={7} exact path="/about" component={About} />),
      (<Route Key={8} exact path="/visualization" component={Visualization} />)
    ];

    return (
      <Router>
        <div>
          <section style = {sectionStyle}>
            <Container fluid={true}>
              <Navbar color="light" light expand="md" fixed="top" collapseOnSelect>
                <NavbarBrand href="/">CongressExposed</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                    <NavLink onClick={this.clearSearch} tag={Link} to="/Home">Home</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink onClick={this.clearSearch} tag={Link} to="/Congresspeople">Congresspeople</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink onClick={this.clearSearch} tag={Link} to="/Bills">Bills</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink onClick={this.clearSearch} tag={Link} to="/Committees">Committees</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink onClick={this.clearSearch} tag={Link} to="/Issue_Category">Issue/Category</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink onClick={this.clearSearch} tag={Link} to="/Visualization">Visualization</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink onClick={this.clearSearch} tag={Link} to="/about">About</NavLink>
                  </NavItem>
                  <NavItem>
                    <Search initialValue={this.state.searchTerm} onChange={this.redirectSearch} />
                  </NavItem>
                </Nav>
                </Collapse>
              </Navbar>
              {(() => {
                if (this.state.searchTerm !== null) {
                  console.log(this.state.searchTerm);
                  return (<Redirect to={`/search/${this.state.searchTerm}`} />);
                } else {
                  return null;
                }})()}
              <Switch>
                {pages}
              </Switch>
            </Container>
          </section>
        </div>
      </Router>
    );
  }
}


export default App;
