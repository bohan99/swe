import React, { Component } from 'react';
import makeCancelable from '../../../Misc/makeCancelable.js';
import { customerAPIFetchAll } from './API.js';
import Trump from "../../../Components/Trump.gif";
import { BarChart } from 'react-charts-d3';

class StateUnprotectedArea extends Component {

    constructor (props) {
        super(props);
        this.state = { data: null };
    }

    componentWillMount() {
        this.apiPromise_ = makeCancelable(customerAPIFetchAll("states").then((data) => this.setState({data: data})));
    }

    render () {
        if (this.state.data === null) {
            return (
                <div>
                  <div style={{textAlign: "center"}}>
                    <img src={Trump} height="35%" width="35%" alt="rocket gif" style={{ marginTop: "1.5%"}} />
                    <p>
                      Loading...
                    </p>
                  </div>
                </div>);
        } else {
            console.log(this.state.data);
            const states = [
                "AZ",
                "CO",
                "DE",
                "FL",
                "GA",
                "HI",
                "ID",
                "IL",
                "IN",
                "IA",
                "KS",
                "KY",
                "LA",
                "MD",
                "ME",
                "MA",
                "MN",
                "MI",
                "MS",
                "MO",
                "MT",
                "NC",
                "NE",
                "NV",
                "NH",
                "NJ",
                "NY",
                "ND",
                "NM",
                "OH",
                "OK",
                "OR",
                "PA",
                "RI",
                "SC",
                "SD",
                "TN",
                "TX",
                "UT",
                "WI",
                "VA",
                "VT",
                "WA",
                "WV",
                "WY",
                "CA",
                "CT",
                "AK",
                "AR",
                "AL",
            ];

            let stateCount = {};
            for (let i = 0; i < states.length; i++) {
                let state = states[i];
                stateCount[state] = 0;
            }

            let statesData = this.state.data;
            for (let i = 0; i < statesData.length; i++) {
                let currentState = statesData[i].code;
                stateCount[currentState] = statesData[i].unprotected_int;
            }


            let dataset = [{ key: "Unprotected Land Area Percentage", values: []}];

            for (let i = 0; i < states.length; i++) {
                let state = states[i];
                dataset[0].values.push({
                    x: state,
                    y: stateCount[state]
                });
            }

            dataset[0].values.sort((f, s) => f.y > s.y);

            console.log(dataset);

            let widthPx = 0.8 * window.innerWidth;
            let heightPx = 0.5 * window.innerHeight;

            return (
                <div>
                  <BarChart
                    width={widthPx}
                    height={heightPx}
                    data={dataset}
                    labels />
               </div>
            );
        }
    }

}

export default StateUnprotectedArea;
