import React, { Component } from 'react';
import makeCancelable from '../../Misc/makeCancelable.js';
import { apiFetch } from '../../Misc/API.js';
import Trump from "../../Components/Trump.gif";
import BubbleChart from '@weknow/react-bubble-chart-d3';

class CongressbyCommittee extends Component {

    constructor (props) {
        super(props);
        this.state = { data: null };
    }

    componentWillMount() {
        this.apiPromise_ = makeCancelable(apiFetch("/api/visualize_1").then((data) => this.setState({data: data})));
    }

    render () {
        if (this.state.data === null) {
            return (
                <div>
                  <div style={{textAlign: "center"}}>
                    <img src={Trump} height="35%" width="35%" alt="rocket gif" style={{ marginTop: "1.5%"}} />
                    <p>
                      Loading...
                    </p>
                  </div>
                </div>);
        } else {
              const visual = this.state.data;
              let visual1_pairs = [];
              for (const key of Object.keys(visual)) {
                visual1_pairs.push({label: key, value: visual[key]});
              }
            return (
                <div>
              <BubbleChart
                  graph= {{
                    zoom: 1,
                    offsetX: 0,
                    offsetY: 0,
                  }}
                  width={1000}
                  height={800}
                  showLegend={true} // optional value, pass false to disable the legend.
                  legendPercentage={20} // number that represent the % of with that legend going to use.
                  legendFont={{
                        family: 'Arial',
                        size: 10,
                        color: '#000',
                        weight: 'bold',
                      }}
                  valueFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#fff',
                        weight: 'bold',
                      }}
                  labelFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#fff',
                        weight: 'bold',
                      }}
                      data = {visual1_pairs}
            />
               </div>
            );
        }
    }

}

export default CongressbyCommittee;
