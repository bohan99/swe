import React, { Component } from 'react';
import makeCancelable from '../../Misc/makeCancelable.js';
import { apiFetch } from '../../Misc/API.js';
import Trump from "../../Components/Trump.gif";
import { ScatterChart } from 'react-charts-d3';

class SocialMediaAge extends Component {

    constructor (props) {
        super(props);
        this.state = { data: null };
    }

    componentWillMount() {
        this.apiPromise_ = makeCancelable(apiFetch("/api/visualize_3").then((data) => this.setState({data: data})));
    }

    render () {
        if (this.state.data === null) {
            return (
                <div>
                  <div style={{textAlign: "center"}}>
                    <img src={Trump} height="35%" width="35%" alt="rocket gif" style={{ marginTop: "1.5%"}} />
                    <p>
                      Loading...
                    </p>
                  </div>
                </div>);
        } else {
             const visual = this.state.data;

              var visual3data = [{key: 'None', values: []}, {key: "Single", values: []},
                                 {key: "Both", values: []}, {key: "Total", values: []}];
              var index = 0;
              for (var dic in visual) {
                console.log(dic);
                for (var key in visual[dic]) {
                  console.log(key);
                  var indArr = visual[dic][key]
                  console.log(indArr);
                  for (var key3val in indArr) {
                    console.log(key3val);
                    visual3data[index].values.push({
                      x: indArr[key3val]["x"],
                      y: indArr[key3val]["y"]
                    });
                  }
                }
                index++;
               }
            return (
                <div>
                <ScatterChart data={visual3data}
                axisConfig={{
                               showXAxis: true,
                               showXAxisLabel: false,
                               xLabel: 'Age',
                               xLabelPosition: 'right',
                               showYAxis: true,
                               showYAxisLabel: true,
                               yLabel: 'Number',
                               yLabelPosition: 'top',
                               }}
                  fluid={true}
                  width={1000}
                  height={400}
                />
               </div>
            );
        }
    }

}

export default SocialMediaAge;
