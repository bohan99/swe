import React, { Component } from 'react';
import { scaleLinear } from 'd3-scale';
import { max } from 'd3-array';
import { Route, Switch } from 'react-router-dom';
import {  Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { select } from 'd3-selection';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import * as d3 from "d3";
import { BarChart,ScatterChart,PieChart, LineChart } from 'react-charts-d3';
import { Committee, Member, Bill } from '../../Misc/Models.js';
import makeCancelable from '../../Misc/makeCancelable.js';
import { absolutePath, apiFetch } from '../../Misc/API.js';
import Trump from "../../Components/Trump.gif";
import AnimalMap from "./customer/AnimalMap.js";
import StateUnprotectedArea from "./customer/StateUnprotectedArea.js";
import HabitatPieChart from "./customer/HabitatPieChart.js";
import CongressbyCommittee from "./CongressbyCommittee.js";
import SocialMediaAge from "./SocialMediaAge.js";
import BillsPassedIssue from "./BillsPassedIssue.js";

class Visualization2 extends Component {
  constructor (props) {
    super(props);
    this.state = {data: null};
    this.apiPromise_ = null;
    this.state.activeTabIndex ="0"
  }
  componentWillMount () {
    this.apiPromise_ = makeCancelable(Promise.all([apiFetch("/api/visualize_1"), apiFetch("/api/visualize_2"), apiFetch("/api/visualize_3")]).then((data) => this.setState({data: data})));
  }
  componentWillUnmount () {
    if (this.apiPromise_ !== null) {
      this.apiPromise_.cancel();
      this.apiPromise_ = null;
    }
  }

  render() {
    function randomNumber() {
        return Math.random() * (40 - 0) + 0;
    }
    if(this.state.data === null) {
      return (
                   <div style={{textAlign: "center"}}>
                      <img src={Trump} height="35%" width="35%" alt="rocket gif" style={{ marginTop: "1.5%"}} />
                      <p>
                          Loading...
                      </p>
                  </div>
                );
    } else {

      return(
        <div>
        <Nav tabs>
          <NavItem>
            <NavLink className={this.state.activeTabIndex === "0" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "0" })}>
              Congress people by Committee
            </NavLink>

          </NavItem>
          <NavItem>
            <NavLink className={this.state.activeTabIndex === "1" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "1" })}>
              Percentage of Bills Passed by Issue
            </NavLink>

          </NavItem>
          <NavItem>
            <NavLink className={this.state.activeTabIndex === "2" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "2" })}>
              Social Media Usage by Age
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={this.state.activeTabIndex === "3" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "3" })}>
              Unprotected Land Area for States
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={this.state.activeTabIndex === "4" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "4" })}>
              Habitats of Insecure Animals and Plants
            </NavLink>
          </NavItem>

          <NavItem>
            <NavLink className={this.state.activeTabIndex === "5" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "5" })}>
              States of Insecure Animals
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTabIndex}>
          <TabPane tabId="0">
          <CongressbyCommittee />

              </TabPane>
          <TabPane tabId="1">
          <BillsPassedIssue />
              </TabPane>

          <TabPane tabId="2">
          <SocialMediaAge />

        </TabPane>
        <TabPane tabId="3">
          <StateUnprotectedArea />
        </TabPane>
        <TabPane tabId="4">
          <HabitatPieChart />
        </TabPane>
        <TabPane tabId="5">
          <AnimalMap />
        </TabPane>
        </TabContent>

        </div>
      );
    }



  }
}
export default class Visualization extends Component {
  render() {
    return (
        <Switch>
        <Route exact path={`${this.props.match.path}`} component={Visualization2} />
        </Switch>
    );
  }
}
