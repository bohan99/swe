import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import { Card, CardBody, CardTitle, Row, Container, Col } from 'reactstrap';
import RenderCards from '../../Components/RenderCards.js';
import ModelPagination from '../../Components/ModelPagination.js';
import ModelManyRelationshipPagination from '../../Components/ModelManyRelationshipPagination.js';
import { CongresspeopleModelPageContent } from '../Congresspeople/Congresspeople.js';
import { BillModelPageContent } from '../Bills/Bills.js';
import TextHighlight from '../../Components/TextHighlight.js';
import Trump from '../../Components/Trump.gif';
import { absolutePath } from '../../Misc/API.js';
import makeCancelable from '../../Misc/makeCancelable.js';
import { Issue, Bill, Member } from '../../Misc/Models.js';
import '../../Components/Card.css';
import '../../Components/IndividualPages.css';

const IssueCard = function (props) {
  const CardImg = styled.img`
    max-width: 100%;
    max-height: 50%;
    min-height: 50%;
    width: auto;
    height: auto;
    object-fit: cover;
  `;
  const image = props.data.getAttribute("picture_url") !== null ? (<CardImg alt={props.data.getAttribute("name")} src={absolutePath(props.data.getAttribute("picture_url"))} />) : null;
  const number_of_bills = (<li>Number Of Bills: {props.data.getAttribute("num_bills")} </li>);
  const number_of_bills_passed = (<li>Number Of Bills Passed: {props.data.getAttribute("num_bills_passed")} </li>);
  const lobbying = props.data.getAttribute("lobbying") !== null ?
        (<li>Lobbying Representative: <TextHighlight filterText={[props.filter["lobbying"]]} text={props.data.getAttribute("lobbying")} /> </li>)
        : null;

  return (
    <Card className="card h-100 w-100">
      {image}
      <CardBody className="cardbody">
        <CardTitle>
          <h4>
            <Link to={{
                    pathname: `/Issue_Category/${props.data.getAttribute("issue_id")}`,
                    state: { filter: props.filter }
                  }}>
              <TextHighlight filterText={[props.filter["name"]]} text={props.data.getAttribute("name")} />
            </Link>
          </h4>
        </CardTitle>
        <ul>
          {lobbying}
          {number_of_bills}
          {number_of_bills_passed}
        </ul>
      </CardBody>
    </Card>
  );
};

class IssueInstancePage extends Component {
  constructor (props) {
    super(props);
    this.state = {data: null};
    this.apiPromise_ = null;
  }

  componentWillMount () {
    this.apiPromise_ = makeCancelable(Issue.fromID(this.props.match.params.issueID).then((data) => this.setState({data: data})));
  }

  componentWillUnmount () {
    if (this.apiPromise_ !== null) {
      this.apiPromise_.cancel();
      this.apiPromise_ = null;
    }
  }

  filter() {
    if (this.props.location.state === undefined || this.props.location.state.filter === undefined) {
      return {};
    } else {
      return this.props.location.state.filter;
    }
  }

  render () {
    if (this.state.data === null) {
      return (
         <div style={{textAlign: "center"}}>
                <img src={Trump} height="35%" width="35%" alt="rocket gif" style={{ marginTop: "1.5%"}} />
                <p>
                    Loading...
                </p>
          </div>
                      );
    } else {
      let filter = this.filter();

      const relatedBills = (<ModelManyRelationshipPagination
                            promiseArray={this.state.data.getRelationship("bills").asArray()}
                            model={Bill}
                            pageContentComponent={BillModelPageContent} />);
      const relatedMembers = (<ModelManyRelationshipPagination
                              promiseArray={this.state.data.getRelationship("members").asArray()}
                              model={Member}
                              pageContentComponent={CongresspeopleModelPageContent} />);
      const image = this.state.data.getAttribute("picture_url") !== null ? (
        <Row>
          <Col sm={2}></Col>
          <Col sm={8} className="text-center"><img alt={this.state.data.getAttribute("name")} src={absolutePath(this.state.data.getAttribute("picture_url"))} /></Col>
          <Col sm={2}></Col>
        </Row>) : null;

      // const bills = this.state.data.bills.map((bill_id) => (<li><Link to={`${this.props.match.url}/../../Bills/${bill_id}`}>{formatBillID(bill_id)}</Link></li>));

      return (
        <Container>
          <br></br>
          <Row>
            <Col sm={2}></Col>
            <Col sm={8}><section id="issueimage">{image}</section></Col>
            <Col sm={2}></Col>
          </Row>
          <p></p><p></p><br></br><p></p>
          <Row>
            <Col sm={4}></Col>
            <Col sm={4} className="text-center"><h1><TextHighlight filterText={[filter["name"]]} text={this.state.data.getAttribute("name")} /></h1></Col>
            <Col sm={4}></Col>
          </Row>
          <br></br>
          <Row>
          <section id="indivHead"><h5>Associated Bills:</h5></section>
          </Row>
          <p></p>
          <Row>
            {relatedBills}
          </Row>
          <Row>
          <section id="indivHead"><h5>Associated Congresspeople:</h5></section>
          </Row>
          <p></p>
          <Row>
            {relatedMembers}
          </Row>
        </Container>);
    }
  }
}

class IssueModelPageContent extends Component {
  render () {
    const cards = this.props.data.map(t => (<IssueCard data={t} filter={this.props.filter} />));
    return (<RenderCards maxPerRow={3} cards={cards} />);
  }
}

class IssueModelPage extends Component {
  render() {
    const renderData = function (data) {
      return (<IssueModelPageContent data={data} filter={this.props.filter} />);
    }.bind(this);

    return (<ModelPagination
            model={Issue}
            render={renderData}
            filter={this.props.filter}
            onFilterChange={this.props.onFilterChange}
            showFilter={this.props.showFilter}
            />);
  }
}

class IssueModelPageController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: {}
    };
  }

  render() {
    return (<IssueModelPage filter={this.state.filter} onFilterChange={(newFilter) => this.setState({filter: newFilter})} showFilter={true} />);
  }
}

export { IssueModelPage, IssueModelPageContent };

export default class Issues extends Component {
  render() {
    return (
      <div>
        <Route exact path={this.props.match.path} component={IssueModelPageController} />
        <Route exact path={`${this.props.match.path}/:issueID`} component={IssueInstancePage} />
      </div>
    );
  }
}
