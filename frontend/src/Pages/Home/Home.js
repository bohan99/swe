import React, { Component } from 'react';
import './Home.css';
import Carousel from 'react-bootstrap/Carousel'
import floor from "./floor.jpg";
import building from "./building.jpg";
import mcconnel from "./mcconnel.jpg";
import pelosi from "./pelosi.jpg";
import senate from "./senate.jpg";
import Background from "../../Components/background.png";
import { Col, Row } from 'reactstrap';

var sectionStyle = {
  width: "auto",
  height: "auto",
  backgroundImage: `url(${Background})`,
  backgroundRepeat: "repeat"
};

class Home extends Component {
  render() {
    return (
      <section style={sectionStyle}>
      <h1><div id="title">Into The Deep: Congress Exposed</div></h1>
      <div style={{ margin: '0em 5em 2em 5em' }}>
        <Carousel id="carousel">
          <Carousel.Item>
            <img
              id="slide-img"
              src={building}
              alt="First Slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              id="slide-img"
              src={floor}
              alt="Second Slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              id="slide-img"
              src={pelosi}
              alt="Third Slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              id="slide-img"
              src={mcconnel}
              alt="Fourth Slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              id="slide-img"
              src={senate}
              alt="Fifth Slide"
            />
          </Carousel.Item>
        </Carousel>
      </div>
      <Row>
        <Col sm={6}>
          <img src={require("./congressLogo.jpg")} alt="logo" id="logo"/>
        </Col>
        <Col sm={6}>
          <h1><div id="titletext"> What Do We Do? </div></h1>
          <div id="text">Even though our government has been around for hundreds of years, there are
            still quite a lot of obscurities, especially surrounding Congress, that may be
            unfamiliar to the public eye. Therefore, our motivation is to unveil the cur-
            tain that lies between the Congress and the people, and in doing so, educate
            the general public on the inner workings of our government along with some
            contemporary issues. Furthermore, studies have shown that most Americans
            are undereducated when it comes to the inner workings of Congress, and this
            is especially true concerning both Congress people and bills. As a result,
            we wish to enlighten people on any obscure issues that Congress may have
            overlooked, blundered, or otherwise failed to provide an effective solution.
          </div>
         </Col>
      </Row>
      <br></br><br></br><br></br>
      <Row>
        <p id="quote" className="quotation"><span id="sans-font">"We the people are the rightful masters of both Congress and the courts,
            not to overthrow the Constitution but to overthrow the men who pervert the Constitution." -- Abraham Lincoln</span></p>
      </Row>
      <br></br><br></br>
      </section>
    );
  }
}
export default Home;
