import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { CongresspeopleModelPage } from "../Congresspeople/Congresspeople.js";
import { BillModelPage } from "../Bills/Bills.js";
import { CommitteeModelPage } from "../Committees/Committees.js";
import { IssueModelPage } from "../Issue/Issue.js";
import { Bill as BillModel, Member as MemberModel, Committee as CommitteeModel, Issue as IssueModel } from "../../Misc/Models.js";

class SiteSearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = { activeTabIndex: "0" };
  }

  render() {

    // copied from Filter.js
    function openFilterableAttributes(attributes) {
      let filterableAttributes = Object.keys(attributes).reduce(function (filtered, key) {
        if (attributes[key].filterable && attributes[key].allowedValues === undefined) {
          filtered[key] = attributes[key];
        }
        return filtered;
      }, {});
      return filterableAttributes;
    }

    let filterObjects = [MemberModel, BillModel, CommitteeModel, IssueModel].map((model) => {
      let schema = model.rawSchema();
      let searchAttributes = Object.keys(openFilterableAttributes(schema.attributes));
      let filterObject = {};
      for (let i = 0; i < searchAttributes.length; i++) {
        filterObject[searchAttributes[i]] = this.props.match.params.searchTerm;
      }
      return filterObject;
    });

    const doNothing = () => {};

    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink className={this.state.activeTabIndex === "0" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "0" })}>
              Congresspeople
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={this.state.activeTabIndex === "1" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "1" })}>
              Bills
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={this.state.activeTabIndex === "2" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "2" })}>
              Committees
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={this.state.activeTabIndex === "3" ? "active" : null}
                     onClick={() => this.setState({ activeTabIndex: "3" })}>
              Issue/Category
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTabIndex}>
          <TabPane tabId="0">
            <CongresspeopleModelPage match={{url: "/Congresspeople"}} filter={filterObjects[0]} onFilterChange={doNothing} showFilter={false} />
          </TabPane>
          <TabPane tabId="1">
            <BillModelPage match={{url: "/Bills"}} filter={filterObjects[1]} onFilterChange={doNothing} showFilter={false} />
          </TabPane>
          <TabPane tabId="2">
            <CommitteeModelPage match={{url: "/Committees"}} filter={filterObjects[2]} onFilterChange={doNothing} showFilter={false} />
          </TabPane>
          <TabPane tabId="3">
            <IssueModelPage match={{url: "/Issue_Category"}} filter={filterObjects[3]} onFilterChange={doNothing} showFilter={false} />
          </TabPane>
        </TabContent>
      </div>
    );
  }
}

export default class SiteSearch extends Component {
  render() {
    return (
        <Switch>
        <Route exact path={`${this.props.match.path}/:searchTerm`} component={SiteSearchPage} />
        </Switch>
    );
  }
}
