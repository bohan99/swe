import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import { Col, Row, Container, Table } from 'reactstrap';
import { makeFullName } from '../Congresspeople/Congresspeople.js';
import ModelPagination from '../../Components/ModelPagination.js';
import TextHighlight from '../../Components/TextHighlight.js';
import ModelManyRelationshipPagination from '../../Components/ModelManyRelationshipPagination.js';
import { CommitteeModelPageContent } from '../Committees/Committees.js';
import { Bill, Committee } from '../../Misc/Models.js';
import makeCancelable from '../../Misc/makeCancelable.js';
import Background from "../../Components/background.png";



const formatBillID = function (billID) {
    const split = billID.split("-");
    const congressNum = split[1];
    const billPart = split[0];
    if (billID.startsWith("hconres")) {
      return `H.Con.Res. ${billPart.substring(7)} - ${congressNum}`;
    } else if (billID.startsWith("hjres")) {
      return `H.J.Res. ${billPart.substring(5)} - ${congressNum}`;
    } else if (billID.startsWith("hres")) {
      return `H.Res. ${billPart.substring(4)} - ${congressNum}`;
    } else if (billID.startsWith("hr")) {
      return `H.R. ${billPart.substring(2)} - ${congressNum}`;
    } else if (billID.startsWith("sconres")) {
      return `S.Con.Res. ${billPart.substring(7)} - ${congressNum}`;
    } else if (billID.startsWith("sjres")) {
      return `S.J.Res. ${billPart.substring(5)} - ${congressNum}`;
    } else if (billID.startsWith("sres")) {
      return `S.Res. ${billPart.substring(4)} - ${congressNum}`;
    } else if (billID.startsWith("s")) {
      return `S. ${billPart.substring(2)} - ${congressNum}`;
    } else {
      return billID;
    }
};

var sectionStyle = {
  width: "auto",
  height: "auto",
  backgroundImage: `url(${Background})`,
  backgroundRepeat: "repeat"
};

const BillRow = function (props) {

  const MaxWidthTD = styled.td`
    width: 35%;
  `;
  const full_title = props.data.getAttribute("full_title") !== null ?
        (<MaxWidthTD><TextHighlight filterText={[props.filter["full_title"]]} text={props.data.getAttribute("full_title")} /> </MaxWidthTD>) : null;
  const short_title = props.data.getAttribute("short_title") !== null && props.data.getAttribute("short_title") !== props.data.getAttribute("full_title") ?
        (<MaxWidthTD><TextHighlight filterText={[props.filter["short_title"]]} text={props.data.getAttribute("short_title")} /> </MaxWidthTD>) : null;
  const display_title = short_title !== null ? short_title : full_title;
  const house_passage_date = props.data.getAttribute("house_passage_date") !== null ?
        (<td><TextHighlight filterText={[props.filter["house_passage_date"]]} text={props.data.getAttribute("house_passage_date")} /> </td>) : (<td>Did not pass House</td>);
  const senate_passage_date = props.data.getAttribute("senate_passage_date") !== null ?
        (<td><TextHighlight filterText={[props.filter["senate_passage_date"]]} text={props.data.getAttribute("senate_passage_date")} /> </td>) : (<td>Did not pass Senate</td>);
  const enacted_date = props.data.getAttribute("enacted_date") !== null ?
        (<td><TextHighlight filterText={[props.filter["enacted_date"]]} text={props.data.getAttribute("enacted_date")} /> </td>) : (<td>Not enacted</td>);
  const vetoed_date = props.data.getAttribute("vetoed_date") !== null ?
        (<td><TextHighlight filterText={[props.filter["vetoed_date"]]} text={props.data.getAttribute("vetoed_date")} /> </td>) : (<td>Not vetoed</td>);

  const display_bill_id = (
    <td>
      <Link to={{
              pathname: `/Bills/${props.data.getAttribute("bill_id")}`,
              state: { filter: props.filter }
            }}>
        {formatBillID(props.data.getAttribute("bill_id"))}
      </Link>
    </td>);

  return (
    <tr>
      {display_bill_id}
      {display_title}
      {house_passage_date}
      {senate_passage_date}
      {enacted_date}
      {vetoed_date}
    </tr>
  );
};

class BillInstancePage extends Component {
  constructor (props) {
    super(props);
    this.state = {data: null};
    this.apiPromise_ = null;
  }

  componentWillMount () {
    this.apiPromise_ = makeCancelable(Bill.fromID(this.props.match.params.billID).then(function (bill_data) {
      bill_data.getRelationship("sponsored_by").get().then(function (sponsoring_member_data) {
        this.setState({data: bill_data, sponsoringMember: sponsoring_member_data});
      }.bind(this));
    }.bind(this)));
  }

  componentWillUnmount () {
    if (this.apiPromise_ !== null) {
      this.apiPromise_.cancel();
      this.apiPromise_ = null;
    }
  }

  filter() {
    if (this.props.location.state === undefined || this.props.location.state.filter === undefined) {
      return {};
    } else {
      return this.props.location.state.filter;
    }
  }

  render () {
    if (this.state.data === null) {
      return (<div>Loading</div>);
    } else {

      let filter = this.filter();

      const full_title = this.state.data.getAttribute("full_title") !== null ?
            (<li>Full Title: <TextHighlight filterText={[filter["full_title"]]} text={this.state.data.getAttribute("full_title")} /> </li>) : null;
      const short_title = this.state.data.getAttribute("short_title") !== null ?
            (<li>Short Title: <TextHighlight filterText={[filter["short_title"]]} text={this.state.data.getAttribute("short_title")} /> </li>) : null;
      const house_passage_date = this.state.data.getAttribute("house_passage_date") !== null ?
            (<li>House Passage Date: <TextHighlight filterText={[filter["house_passage_date"]]} text={this.state.data.getAttribute("house_passage_date")} /> </li>) : (<li>Did not pass House</li>);
      const senate_passage_date = this.state.data.getAttribute("senate_passage_date") !== null ?
            (<li>Senate Passage Date: <TextHighlight filterText={[filter["senate_passage_date"]]} text={this.state.data.getAttribute("senate_passage_date")} /> </li>) : (<li>Did not pass Senate</li>);
      const enacted_date = this.state.data.getAttribute("enacted_date") !== null ?
            (<li>Enacted Date: <TextHighlight filterText={[filter["enacted_date"]]} text={this.state.data.getAttribute("enacted_date")} /> </li>) : (<li>Was not enacted</li>);
      const vetoed_date = this.state.data.getAttribute("vetoed_date") !== null ?
            (<li>Vetoed Date: <TextHighlight filterText={[filter["vetoed_date"]]} text={this.state.data.getAttribute("vetoed_date")} /> </li>) : (<li>Was not vetoed</li>);
      const summary = this.state.data.getAttribute("summary") !== null ?
            (<p><TextHighlight filterText={[filter["summary"]]} text={this.state.data.getAttribute("summary")} /></p>) : (<p>No summary</p>);
      const sponsoringMember = this.state.sponsoringMember !== null ? (<p>Sponsored by <Link to={`/Congresspeople/${this.state.sponsoringMember.getAttribute("member_id")}`}>{
        makeFullName(this.state.sponsoringMember.getAttribute("first_name"), this.state.sponsoringMember.getAttribute("middle_name"), this.state.sponsoringMember.getAttribute("last_name"))}</Link></p>) : null;

      const relatedCommittees = (<ModelManyRelationshipPagination
                                 promiseArray={this.state.data.getRelationship("committees").asArray()}
                                 model={Committee}
                                 pageContentComponent={CommitteeModelPageContent} />);
      return (
        <Container>
          <Row>
            <Col sm={4}></Col>
            <Col sm={4} className="text-center"><h1>{formatBillID(this.state.data.getAttribute("bill_id"))}</h1></Col>
            <Col sm={4}></Col>
          </Row>
          <Row>
            <ul>
              {full_title}
              {short_title}
              {house_passage_date}
              {senate_passage_date}
              {enacted_date}
              {vetoed_date}
            </ul>
          </Row>
          <Row>
            {summary}
          </Row>
          <Row>
            {sponsoringMember}
          </Row>
          <Row>
            Sponsoring committees:
          </Row>
          <Row>
            {relatedCommittees}
          </Row>
        </Container>);
    }
  }
}

class BillModelPageContent extends Component {
  render() {
    const rows = this.props.data.map((t, index) => (<BillRow key={index} data={t} filter={this.props.filter} />));
    return (
      <Table responsive={true} borderless={false}>
        <thead>
          <tr className="text-center">
            <th>Bill ID</th>
            <th>Name</th>
            <th>Passed House</th>
            <th>Passed Senate</th>
            <th>Enacted</th>
            <th>Vetoed</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </Table>
    );
  }
}

class BillModelPage extends Component {


  render() {
    const renderData = (data) => (<BillModelPageContent data={data} filter={this.props.filter} />);

    return (<ModelPagination
            model={Bill}
            render={renderData}
            filter={this.props.filter}
            onFilterChange={this.props.onFilterChange}
            showFilter={this.props.showFilter}
            />);
  }
}

export { formatBillID, BillModelPage, BillModelPageContent };

class BillModelPageController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: {}
    };
  }

  render() {
    return (<BillModelPage filter={this.state.filter} onFilterChange={(newFilter) => this.setState({filter: newFilter})} showFilter={true} />);
  }
}

export default class Bills extends Component {
  render() {
    return (
      <div>
        <section style = {sectionStyle}>
        <Route exact path={this.props.match.path} component={BillModelPageController} />
        <Route exact path={`${this.props.match.path}/:billID`} component={BillInstancePage} />
        </section>
      </div>
    );
  }
}
