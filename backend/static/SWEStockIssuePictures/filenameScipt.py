from os import rename, listdir


for filename in listdir("."):
    if filename != ".DS_Store" and filename != "filenameScipt.py":
        newname = filename.split(".jpg")
        # print(newname[0] + '.jpg')
        rename(filename, newname[0] + ".jpg")
