import flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from sqlalchemy.orm import load_only
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String

application = flask.Flask(__name__)
# allow cross domains
CORS(application)

application.config["DEBUG"] = False
application.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql://master:masterpassword@mydb.ceoq3fxzofj3.us-east-1.rds.amazonaws.com/testdb"
application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(application)
db.Model.metadata.reflect(db.engine)


class Bill(db.Model):
    __table__ = db.Model.metadata.tables["Bill"]
    cosponsors = db.relationship(
        "Member",
        secondary=db.Model.metadata.tables["Bill_Cosponsor"],
        backref=db.backref("cosponsored_bills", lazy="dynamic"),
        lazy="dynamic",
    )
    sponsored_by = db.relationship(
        "Member", backref=db.backref("sponsored_bills", lazy="dynamic")
    )
    committees = db.relationship(
        "Committee",
        secondary=db.Model.metadata.tables["Bill_Committee"],
        backref=db.backref("bills", lazy="dynamic"),
        lazy="dynamic",
    )
    subcommittees = db.relationship(
        "Subcommittee",
        secondary=db.Model.metadata.tables["Bill_Subcommittee"],
        backref=db.backref("bills", lazy="dynamic"),
        lazy="dynamic",
    )
    issues = db.relationship(
        "Issue",
        secondary=db.Model.metadata.tables["Issue_Bill"],
        backref=db.backref("bills", lazy="dynamic"),
        lazy="dynamic",
    )

    def serialize(self, links=False):
        attributes = {
            "bill_id": self.bill_id,
            "short_title": self.short_title,
            "full_title": self.full_title,
            "summary": self.summary,
            "house_passage_date": serialize_date(self.house_passage_date),
            "senate_passage_date": serialize_date(self.senate_passage_date),
            "enacted_date": serialize_date(self.enacted_date),
            "vetoed_date": serialize_date(self.vetoed_date),
            "congress": self.congress,
        }

        if links:
            relationships = {
                "cosponsors": [
                    x.member_id
                    for x in self.cosponsors.options(load_only("member_id")).all()
                ],
                "sponsored_by": None
                if self.sponsored_by is None
                else self.sponsored_by.member_id,
                "committees": [
                    x.committee_id
                    for x in self.committees.options(load_only("committee_id")).all()
                ],
                "subcommittees": [
                    x.subcommittee_id
                    for x in self.subcommittees.options(
                        load_only("subcommittee_id")
                    ).all()
                ],
                "issues": [
                    x.issue_id for x in self.issues.options(load_only("issue_id")).all()
                ],
                "rollcalls": [r.rollcall_id for r in self.rollcalls],
            }

            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


class Committee(db.Model):
    __table__ = db.Model.metadata.tables["Committee"]
    roles = db.relationship(
        "MemberRoleCommittee",
        backref=db.backref("member_role_committee"),
        lazy="dynamic",
    )

    def serialize(self, links=False):
        attributes = {
            "committee_id": self.committee_id,
            "committee_code": self.committee_code,
            "congress": self.congress,
            "name": self.name,
            "website": self.website,
            "chair_party": self.chair_party,
        }

        if links:
            relationships = {
                "bills": [
                    x.bill_id for x in self.bills.options(load_only("bill_id")).all()
                ],
                "subcommittees": [
                    x.subcommittee_id
                    for x in self.subcommittees.options(
                        load_only("subcommittee_id")
                    ).all()
                ],
                "member_roles": [x.member_role_id for x in self.member_roles],
                "members": [
                    x.member_id
                    for x in Member.query.join(MemberRole)
                    .join(MemberRoleCommittee)
                    .join(Committee)
                    .filter(MemberRoleCommittee.committee_id == self.committee_id)
                    .filter(
                        MemberRole.member_role_id == MemberRoleCommittee.member_role_id
                    )
                    .all()
                ],
            }

            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


class Subcommittee(db.Model):
    __table__ = db.Model.metadata.tables["Subcommittee"]
    parent_committee = db.relationship(
        "Committee", backref=db.backref("subcommittees", lazy="dynamic"), lazy="joined"
    )

    def serialize(self, links=False):
        attributes = {
            "subcommittee_id": self.subcommittee_id,
            "subcommittee_code": self.subcommittee_code,
            "name": self.name,
            "website": self.website,
            "chair_party": self.chair_party,
        }

        if links:
            relationships = {
                "bills": [
                    x.bill_id for x in self.bills.options(load_only("bill_id")).all()
                ],
                "parent_committee": self.parent_committee.committee_id,
            }
            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


class IssueNumBills(db.Model):
    __tablename__ = "IssueNumBills"
    issue_id = Column(Integer, primary_key=True)
    name = Column(String)
    picture_url = Column(String)
    lobbying = Column(String)
    num_bills_passed = Column(Integer)
    num_bills = Column(Integer)

    def serialize(self, *args, **kwargs):
        return (
            Issue.query.filter(Issue.issue_id == self.issue_id)
            .first()
            .serialize(*args, **kwargs)
        )


class Issue(db.Model):
    __table__ = db.Model.metadata.tables["Issue"]

    def serialize(self, links=False):
        issue_num_bills = IssueNumBills.query.filter(
            IssueNumBills.issue_id == self.issue_id
        ).first()
        attributes = {
            "issue_id": self.issue_id,
            "name": self.name,
            "lobbying": self.lobbying,
            "picture_url": self.picture_url,
            "num_bills": issue_num_bills.num_bills,
            "num_bills_passed": issue_num_bills.num_bills_passed,
        }

        if links:
            relationships = {
                "bills": [
                    x.bill_id for x in self.bills.options(load_only("bill_id")).all()
                ],
                "members": [
                    x.member_id
                    for x in self.members.options(load_only("member_id")).all()
                ],
            }
            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


class Member(db.Model):
    __table__ = db.Model.metadata.tables["Member"]
    issues = db.relationship(
        "Issue",
        secondary=db.Model.metadata.tables["Issue_Member"],
        backref=db.backref("members", lazy="dynamic"),
        lazy="dynamic",
    )
    roles = db.relationship("MemberRole", backref=db.backref("member"), lazy="dynamic")

    def serialize(self, links=False):
        attributes = {
            "member_id": self.member_id,
            "first_name": self.first_name,
            "middle_name": self.middle_name,
            "last_name": self.last_name,
            "twitter_account": self.twitter_account,
            "facebook_account": self.facebook_account,
            "party": self.party,
            "biography": self.biography,
            "date_of_birth": serialize_date(self.date_of_birth),
            "website": self.website,
            "picture_url": self.picture_url,
        }

        if links:
            relationships = {
                "roles": [
                    r.member_role_id
                    for r in self.roles.options(load_only("member_role_id")).all()
                ],
                "sponsored_bills": [
                    x.bill_id
                    for x in self.sponsored_bills.options(load_only("bill_id")).all()
                ],
                "cosponsored_bills": [
                    x.bill_id
                    for x in self.cosponsored_bills.options(load_only("bill_id")).all()
                ],
                "related_issues": [
                    r.issue_id for r in self.issues.options(load_only("issue_id")).all()
                ],
                "roll_call_positions": [
                    r.rollcallposition_id
                    for r in self.roll_call_positions.options(
                        load_only("rollcallposition_id")
                    ).all()
                ],
            }
            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


class MemberRole(db.Model):
    __table__ = db.Model.metadata.tables["Member_Role"]
    committee_assignments = db.relationship(
        "MemberRoleCommittee", backref=db.backref("member_role"), lazy="joined"
    )
    subcommittee_assignments = db.relationship(
        "MemberRoleSubcommittee", backref=db.backref("member_role"), lazy="joined"
    )

    def serialize(self, links=False):
        attributes = {
            "member_role_id": self.member_role_id,
            "member_id": self.member_id,
            "congress": self.congress,
            "chamber": self.chamber,
            "title": self.title,
            "short_title": self.short_title,
            "party": self.party,
            "leadership_role": self.leadership_role,
            "district": self.district,
            "state": self.state,
            "missed_votes_percent": self.missed_votes_percent,
            "votes_with_party_percent": self.votes_with_party_percent,
            "start_date": serialize_date(self.start_date),
            "end_date": serialize_date(self.end_date),
        }

        if links:
            relationships = {
                "committee_assignments": [
                    c.member_role_committee_id for c in self.committee_assignments
                ],
                "subcommittee_assignments": [
                    c.member_role_subcommittee_id for c in self.subcommittee_assignments
                ],
            }
            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


class MemberRoleCommittee(db.Model):
    __table__ = db.Model.metadata.tables["Member_Role_Committee"]
    committee = db.relationship(
        "Committee", backref=db.backref("member_roles"), lazy="joined"
    )

    def serialize(self, links=False):
        attributes = {
            "member_role_committee_id": self.member_role_committee_id,
            "member_role_id": self.member_role_id,
            "committee_id": self.committee_id,
            "title": self.title,
            "rank_in_party": self.rank_in_party,
        }

        if links:
            relationships = {"committee": self.committee.committee_id}
            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


class MemberRoleSubcommittee(db.Model):
    __table__ = db.Model.metadata.tables["Member_Role_Subcommittee"]
    subcommittee = db.relationship(
        "Subcommittee", backref=db.backref("member_roles"), lazy="joined"
    )

    def serialize(self, links=False):
        attributes = {
            "member_role_subcommittee_id": self.member_role_subcommittee_id,
            "member_role_id": self.member_role_id,
            "subcommittee_id": self.subcommittee_id,
            "title": self.title,
            "rank_in_party": self.rank_in_party,
        }

        if links:
            relationships = {"subcommittee": self.subcommittee.subcommittee_id}
            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


class RollCall(db.Model):
    __table__ = db.Model.metadata.tables["RollCall"]
    bill = db.relationship("Bill", backref=db.backref("rollcalls"), lazy="joined")

    def serialize(self, links=False):
        attributes = {
            "congress": self.congress,
            "rollcall_id": self.rollcall_id,
            "bill_id": self.bill_id,
            "roll_call_date": self.roll_call_date,
            "session_number": self.session_number,
            "chamber": "{}".format(self.chamber),
            "roll_call_number": self.roll_call_number,
        }

        if links:
            relationships = {"bill": self.bill.bill_id}
            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


class RollCallPosition(db.Model):
    __table__ = db.Model.metadata.tables["RollCallPosition"]
    rollcall = db.relationship("RollCall", backref=db.backref("positions"))
    member = db.relationship(
        "Member",
        backref=db.backref("roll_call_positions", lazy="dynamic"),
        lazy="joined",
    )

    def serialize(self, links=False):
        attributes = {
            "rollcallposition_id": self.rollcallposition_id,
            "rollcall_id": self.rollcall_id,
            "member_id": self.member_id,
            "vote_position": self.vote_position,
            "dw_nominate": self.dw_nominate,
        }

        if links:
            relationships = {
                "member": self.member.member_id,
                "rollcall": self.rollcall.rollcall_id,
            }
            return {"attributes": attributes, "relationships": relationships}
        else:
            return {"attributes": attributes}


# Helper method
def serialize_date(s):
    if s is None:
        return None
    else:
        return str(s)
